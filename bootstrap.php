<?php
   $items = [
       "app/models/Informacao.php",
       "app/models/Contato.php",
       "app/functions/util.php",
       "app/functions/validators/AbstractValidator.php",
       "app/functions/validators/Validator.php",
       "app/functions/validators/EmailValidator.php",
       "app/functions/validators/IntervalValidator.php",
       "app/functions/validators/SizeValidator.php"
   ];

   foreach ($items as $key => $value) {
     require_once($value);
   }

?>
