<?php

namespace models {

   class Informacao{
      private $tipo;
      private $valor;

      function __construct($tipo, $valor){
         echo "Nova informação";
         $this->tipo = $tipo;
         $this->valor = $valor;
      }
      
      public function getTipo()
      {
        return $this->tipo;
      }

      public function setTipo($tipo)
      {
        $this->tipo = $tipo;

        return $this;
      }

      public function getValor()
      {
        return $this->valor;
      }

      public function setValor($valor)
      {
        $this->valor = $valor;
        return $this;
      }

   }
}

?>
