<?php
namespace models {

   class Contato{

      private $id;
      private $nome;
      private $sobrenome;
      private $apelido;
      private $informacoes;

      function __construct($nome){
         echo "Nova Pessoa";
         $this->nome = $nome;
      }

      public function getId()
      {
          return $this->id;
      }

      public function setId($id)
      {
          $this->id = $id;
          return $this;
      }

      public function getNome()
      {
          return $this->nome;
      }

      public function setNome($nome)
      {
          $this->nome = $nome;
          return $this;
      }

      public function getSobrenome()
      {
          return $this->sobrenome;
      }

      public function setSobrenome($sobrenome)
      {
          $this->sobrenome = $sobrenome;
          return $this;
      }

      public function getApelido()
      {
          return $this->apelido;
      }

      public function setApelido($apelido)
      {
          $this->apelido = $apelido;
          return $this;
      }

      public function getInformacoes()
      {
          return $this->informacoes;
      }

      public function setInformacoes($informacoes)
      {
          $this->informacoes = $informacoes;
          return $this;
      }

   }

}

?>
