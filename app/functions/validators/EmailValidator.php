<?php
class EmailValidator extends AbstractValidator{

   function __construct(){
   }

   public function validate($value){
      return filter_var($value, FILTER_SANITIZE_EMAIL);
   }

   public function getMessage(){
      return "O e-mail informado é inválido";
   }
}
?>
