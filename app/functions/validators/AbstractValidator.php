<?php
abstract class AbstractValidator {
   abstract public function validate($value);
   abstract public function getMessage();
}

 ?>
