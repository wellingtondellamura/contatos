<?php

class SizeValidator extends AbstractValidator{
   private $min;
   private $max;

   function __construct($min, $max){
      $this->min = $min;
      $this->max = $max;
   }

   public function validate($value){
      $len = strlen($value);
      return ($len<=$this->max && $len>=$this->min);
   }

   public function getMessage(){
      return "O tamanho do texto deve ser entre $this->min e $this->max.";
   }
}

?>
