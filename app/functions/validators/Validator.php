<?php
class Validator{
   function __construct(){

   }
   private $valid;

   private $messages;

   public function validate(array $fields) {
      $this->valid = true;
      $request = request();
	   $validate = [];
      $this->messages = [];
      foreach ($fields as $field => $validators) {
         $validate[$field] = $request[$field];
         $this->messages[$field] = "";
         foreach ($validators as $validator){
            if (!$validator->validate($validate[$field])){
               $this->messages[$field] .= $validator->getMessage();
               $this->valid = false;
            }
         }
      }
	   return (object) $validate;
   }

    public function isValid() {
      return $this->valid;
    }

    public function getMessages() {
        return $this->messages;
    }

    public function setMessages($messages) {
        $this->messages = $messages;
        return $this;
    }

}

?>
