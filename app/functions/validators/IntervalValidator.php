<?php

class IntervalValidator extends AbstractValidator{
   private $min;
   private $max;

   function __construct($min, $max){
      $this->min = $min;
      $this->max = $max;
   }

   public function validate($value){
      return ($value<=$this->max && $value>=$this->min);
   }

   public function getMessage(){
      return "O valor deve estar entre $this->min e $this->max.";
   }
}

?>
