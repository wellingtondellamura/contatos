<?php
   require_once "../../bootstrap.php";

   //dd(request());
   $validator = new Validator();
   $fields = $validator->validate([
      'nome'  => [new SizeValidator(1, 10)],
      'idade' => [new IntervalValidator(1, 100)],
      'email' => [new EmailValidator()]
   ]);

   dd([$fields, $validator])
?>
